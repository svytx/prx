<?php
class Rtr {
    static function route() {
        $x = explode('.', $_SERVER['HTTP_HOST']);
        $len = count($x);
        $f = ucfirst(strtolower($x[$len-2].'_'.$x[$len-1]));

        require_once $_SERVER['DOCUMENT_ROOT'].'/host/'.
            lcfirst($f).'.php';
        (new $f)->go();
    }
}

(new Rtr())->route();
?>
