<?php
set_include_path(get_include_path().PATH_SEPARATOR.'lib/netdns2');
require_once 'Net/DNS2.php';

class Dns {
    function ips($d) {
        $r = new Net_DNS2_Resolver(
            array(
                'nameservers'       => array('8.8.8.8', '8.8.4.4'),
                'cache_type'        => 'file',
                'cache_file'        => getcwd().'/cache/dns',
                'cache_size'        => 10000000,
                'cache_serializer'  => 'json'
            )
        );

        try {
            $res = $r->query($d, 'A');
        } catch (Net_DNS2_Exception $e) {
            die('Error: '.$e->getMessage());
        }

        $ips = array();
        foreach ($res->answer as $ip) $ips[] = $ip->address;

        return $ips;
    }
}
?>
