# prx

## About

Proxy and data parsing.
Handles '.fastpic.ru' at the moment.

## Requirements

* DNS server to translate domains to your web server.
* HTTP server which handles these domains and routes them to index.php
* PHP
* Writeable ./cache/dns file for caching DNS responses (optional)
* svytx/Sugar/crwlr.php

