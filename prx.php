<?php
require_once 'crwl.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/dns.php';

class Prx extends Sugar\Crwl {
    function fetch($host, $uri) {
        $ips = (new Dns)->ips($host);
        $url = 'http://'.$ips[0].$uri;
        
        $this->setCurlOpts(array(
            CURLOPT_HTTPHEADER => array('Host: '.$host)
        ));

        $this->setUrl($url);
        
        $r = $this->get(true);

        header('Content-Type: '.$r[1]['content_type']);
        
        return $r;
    }
}
?>
