<?php
require_once  $_SERVER['DOCUMENT_ROOT'].'/dns.php';
require_once  $_SERVER['DOCUMENT_ROOT'].'/prx.php';

class Fastpic_ru extends Prx {
    private function parse_url() {
        $x = explode('/', $_SERVER['REQUEST_URI']);
        $r = array($_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']);
        
        if (count($x) != 6 || $x[1] != 'view') return $r;
        
        $magic1 = substr($x[5], -11, 2);
        $magic2 = substr($x[5], 0, -5);
        $r = array(
            'i'.$x[2].'.fastpic.ru',
            '/big/'.$x[3].'/'.$x[4].'/'.$magic1.'/'.$magic2
        );

        return $r;
    }

    function go() {
        list($host, $uri) = $this->parse_url();
        list($r, $i) = $this->fetch($host, $uri);

        echo $r;
    }
}
?>
